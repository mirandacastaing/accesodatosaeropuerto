package castaing.miranda.accesobd;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

public class Conector {

    private static AccesoBD connectorBD = null;
    private static final String NOMBRE_ARCHIVO = "Archivo.txt";

    public static AccesoBD getConnector() throws SQLException, Exception {
        if (connectorBD == null) {
            try {
                FileReader reader = new FileReader(NOMBRE_ARCHIVO);
                BufferedReader buffer = new BufferedReader(reader);
                connectorBD = new AccesoBD(buffer.readLine(), buffer.readLine());
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return connectorBD;
    }

}
